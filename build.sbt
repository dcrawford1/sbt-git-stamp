/* basic project info */
name := "sbt-git-stamp"

organization := "com.atlassian.labs"

version := "0.1.0-SNAPSHOT"

sbtPlugin := true

description := "An SBT plugin that will stamp the MANIFEST.MF file in the output artifact with some basic git information."

homepage := Some(url("https://bitbucket.org/pkaeding/sbt-git-stamp"))

startYear := Some(2013)

/* scala versions and options */
scalaVersion := "2.9.2"

crossScalaVersions := Seq("2.9.2", "2.10.0", "2.10.1")

CrossBuilding.crossSbtVersions := Seq("0.11.2", "0.11.3", "0.12.0")

offline := false

scalacOptions ++= Seq("-deprecation", "-unchecked")

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

/* dependencies */
libraryDependencies ++= Seq (
  "com.github.nscala-time" %% "nscala-time" % "0.2.0",
  "org.eclipse.jgit" % "org.eclipse.jgit" % "2.2.0.201212191850-r"
)

/* you may need these repos */
resolvers ++= Seq(
  // Resolvers.sonatypeRepo("snapshots")
  // Resolvers.typesafeIvyRepo("snapshots")
  // Resolvers.typesafeIvyRepo("releases")
  // Resolvers.typesafeRepo("releases")
  // Resolvers.typesafeRepo("snapshots")
  // JavaNet2Repository,
  // JavaNet1Repository
)

/* sbt behavior */
logLevel in compile := Level.Warn

traceLevel := 5

releaseSettings

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra := (
  <developers>
    <developer>
      <id>pkaeding</id>
      <name>Patrick Kaeding</name>
      <email>pkaeding@atlassian.com</email>
      <!-- <url></url> -->
    </developer>
  </developers>
)

// Josh Suereth's step-by-step guide to publishing on sonatype
// httpcom://www.scala-sbt.org/using_sonatype.html

